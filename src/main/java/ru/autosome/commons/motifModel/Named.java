package ru.autosome.commons.motifModel;

public interface Named {
  public String getName();
  public void setName(String name);
}
