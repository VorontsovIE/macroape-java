package ru.autosome.commons.motifModel.types;

public enum DataModel {
  PCM, PPM, PWM
}
